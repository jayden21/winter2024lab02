public class Calculator{
	public static int add(int num1, int num2){
		return num1+num2;
	}
	public static double sqrt(int num){
		return Math.sqrt(num);
	}
	public static int random(){
		java.util.Random Rand = new java.util.Random();
		return Rand.nextInt();
	}
	public static double divide(double num1, double num2){
		if (num1==0 || num2==0)
			return 0;
		return num1/num2;
	}
}